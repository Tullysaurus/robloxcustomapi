from flask_restful import Resource, Api
from playwright.sync_api import sync_playwright
from threading import Thread
from flask import Flask
import time

app = Flask(__name__)
api = Api(app)

RSRunning = False

class Home(Resource):

  def get(self):
    return {"Page": "/"}


class ApiHome(Resource):

  def get(self):
    return {"Page": "/api/"}


class ApiRobloSecurity(Resource):

  def get(self, user, passw):
    global RSRunning, Username, Password
    Username = user
    Password = passw
    RSRunning = True
    RSecurity = GetRobloSecurity(Username, Password)
    while RSRunning: time.sleep(1)
    return {
      "Page": f"/api/security/{user}/{passw}",
      "RobloSecurity": RSecurity,
      "Success": False if RSecurity == "" else True
    }


def GetRobloSecurity(Username, Password):
    global RSRunning
    cookies = {}
    with sync_playwright() as p:
        browser = p.webkit.launch(headless=False)
        context = browser.new_context()
        page = context.new_page()
        page.goto("https://www.roblox.com/login")
        page.query_selector("#login-username").fill(Username)
        page.query_selector("#login-password").fill(Password)
        page.query_selector("#login-button").click()
        time.sleep(1)
        page.wait_for_url("https://*.roblox.com/home")
        cookies = context.cookies()
        browser.close()
    RobloSecurity = ""
    for CPair in cookies:
        if CPair["name"] == ".ROBLOSECURITY":
            RobloSecurity = CPair["value"]
    RSRunning = False
    return RobloSecurity


api.add_resource(Home, "/")
api.add_resource(ApiHome, "/api/")
api.add_resource(ApiRobloSecurity,
                 "/api/roblosecurity/<string:user>/<string:passw>")


def run():
  # app.run(host='0.0.0.0', port=5555)
  app.run(port=5555)


Thread(target=run).start()